



#Sujet 3 : Fonctions

### Notions à acquérir 

Fonctions, passage de paramètres par valeurs.

Pour l'implantation dans un langage de programmation, vous coderez les
fonctions dans un seul fichier que la fonction principale vous permettra
de tester.

### Maximum de trois nombres

1.  Ecrire la fonction `max2(a, b : entier) retourne entier` qui retourne la plus grande des deux valeurs ` a ` et ` b `.

2.  En utilisant la fonction précédente, écrire la fonction
    ` max3(a,b,c : entier) retourne entier` qui retourne la
    plus grande des trois valeurs ` a `, ` b ` et ` c`.

3.  Ecrire une fonction `testMax()  void` qui demande d'abord à
    l'utilisateur deux nombres entiers et affiche leur maximum, puis
    demande trois nombres entiers et affiche leur maximum. Une fonction
    ne retournant rien est aussi appelée *procédure*.

### Pyramide simple

On souhaite afficher à l'écran une pyramide de hauteur donnée ` h`
constituée de lignes répétant un caractère donné ` c`. Par exemple, si
`h = 5` et `c = *`, on obtient la pyramide suivante :

          *
         ***
        *****
       *******
      *********


1.  Ecrire d'abord une fonction
    `repeteCarac (nb : entier, car : caractère)  void` qui
    permet d'afficher `nb` caractères `car` à partir de la position
    courante du curseur sur le terminal.

2.  En utilisant la fonction précédente, écrire la procédure :
    ` pyramideSimple (h : entier, c : caractère) void`
    qui permet d'afficher à l'écran une pyramide de hauteur ` h`
    constituée de lignes répétant le caractère ` c`.

3.  Ecrire une fonction `testPyramideSimple() retourne void` qui demande
    à l'utilisateur une hauteur de pyramide et un caractère, puis
    affiche une pyramide simple.


### Pyramide élaborée

Affichons maintenant une pyramide un peu plus élaborée que la
précédente.

1.  Ecrire une fonction
    `afficheNombresCroissants (nb1, nb2 : entier)  void` qui
    permet d'afficher à l'écran sur une même ligne les `chiffres`
    représentant les unités des nombres allant de `nb1` à `nb2` en ordre
    croissant si `nb1` <= `nb2`, et ne fait rien sinon.

    Par exemple, si `nb1` = 88 et `nb2` = 91, la fonction affiche
    `8 9 0 1`.

2.  Ecrire une procédure
    `afficheNombresDecroissants (nb1, nb2 : entier)  void` qui
    permet d'afficher à l'écran sur une même ligne les chiffres
    représentant les unités des nombres allant de `nb1` à `nb2` en ordre
    décroissant si `nb1` <= `nb2`, et ne fait rien sinon.

    Par exemple, si `nb1` = 88 et `nb2` = 91, la fonction affiche
    `1 0 9 8`.

3.  En utilisant les fonctions précédentes (voire celles de l'exercice
    précédent), écrire une fonction
    `pyramideElaboree (h : entier)  void` qui permet de
    représenter à l'écran la pyramide suivante si `h = 10` :

                          1
                        2 3 2
                      3 4 5 4 3
                    4 5 6 7 6 5 4
                  5 6 7 8 9 8 7 6 5
                6 7 8 9 0 1 0 9 8 7 6
              7 8 9 0 1 2 3 2 1 0 9 8 7
            8 9 0 1 2 3 4 5 4 3 2 1 0 9 8
          9 0 1 2 3 4 5 6 7 6 5 4 3 2 1 0 9
        0 1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1 0

4.  Ecrire une fonction de test `testPyramideElaboree()  void`.


### Nombre de chiffres composant un nombre

1.  Ecrire la fonction `nbChiffres (n : entier) retourne entier`, qui
    calcule et retourne le nombre de chiffres que comporte l'écriture,
    en base 10, de l'entier naturel ` n`.

    Indication : diviser ` n` par 10 itérativement.

2.  Ecrire la fonction `nbChiffresDuCarre (n : entier) retourne entier`,
    qui calcule et retourne le nombre de chiffres que comporte
    l'écriture du nombre `n^2`.


### Nombres amis

1.  Deux nombres ` p` et ` q` sont dits *amis* si ` p` vaut la somme des
    diviseurs de ` q` (autres que ` q`) et ` q` vaut la somme des diviseurs
    de ` p` (autres que ` p`). Par exemple 220 et 284.

    Ecrire une fonction qui détermine si deux nombres entiers ` p` et ` q`
    sont amis.

2.  Ecrire une fonction qui permet d'afficher à l'écran les couples de
    nombres amis parmi les nombres inférieurs ou égaux à un entier
    donné.



### Carré parfait

Ecrire les fonctions suivantes en utilisant si possible les fonctions
définies dans les questions précédentes.

1.  Ecrire une fonction `racineParfaite` qui retourne la racine carrée
    entière ` n` d'un nombre entier ` c` donné, si ` c` est un carré
    parfait, c'est-à-dire si `c = n * n`. Sinon la fonction
    retourne `-1`.

2.  Ecrire une fonction `estCarreParfait` qui détermine si un entier
    donné est un carré parfait.

3.  Ecrire une fonction qui détermine si deux entiers donnés peuvent
    être les côtés de l'angle droit d'un triangle rectangle dont les 3
    côtés sont des nombres entiers.



### Nombre de triangles rectangles avec côtés entiers

Ecrire une fonction qui calcule le nombre de triangles rectangles dont
les côtés sont des nombres entiers et dont le périmètre est inférieur à
un nombre ` n` passé en paramètre.


### Nombres syracusiens

Un nombre ` n` est syracusien si, en répétant l'opération suivante, on
obtient l'entier 1 au bout d'un nombre fini d'étapes :

-   si ` n` est pair, il est remplacé par sa moitié ;

-   si ` n` est impair, il est remplacé par `3n + 1`.

1.  Parmi les entiers de 1 à 10, lesquels sont syracusiens ?

2.  Ecrire, si c'est possible, une fonction qui, étant donné un entier
    strictement positif ` n`, détermine s'il est syracusien.

3.  Ecrire une fonction qui, étant donnés deux entiers strictement
    positifs ` n` et `nbMaxOp`, retourne `vrai` si on obtient 1 à partir
    de ` n` en répétant au plus `nbMaxOp` fois l'opération ci-dessus, et
    `faux` sinon.

    Que peut-on dire de ` n` si le résultat retourné est vrai ? Que
    peut-on dire de ` n` si le résultat retourné est faux ?



