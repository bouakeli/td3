int max2(int a, int b){
    if (a > b){
        return a;
    }
    return b;
}
int max3(int a, int b, int c){
    if (max2(a, b)  > c){
        return max2(a, b);
    }
    return c;
}
void testMax(){
    Ut.afficher("Saisir deux nombres entier ");
    int a = Ut.saisirEntier();
    int b = Ut.saisirEntier();
    Ut.afficherSL("Le maximum est "+max2(a, b));
    Ut.afficher("Saisir 3 entier ");
    a = Ut.saisirEntier();
    b = Ut.saisirEntier();
    int c = Ut.saisirEntier();
    Ut.afficher("Le maximum est "+ max3(a, b, c));

}
void repeteCar(int nb, char car){
    for (int i = 0; i < nb; i++) {
        Ut.afficher(car);
    }
}
void pyramideSimple(int h, char c){
    for (int i = 0; i <= h; i++) {
        repeteCar(h-i, ' ');
        repeteCar(2*i-1, c);
        Ut.afficherSL(' ');
    }
}
void testPyramideSimple(){
    Ut.afficher("Saisir une hauteur");
    int h = Ut.saisirEntier();
    Ut.afficher("Saisir un caractère");
    char c = Ut.saisirCaractere();
    for (int i = 0; i <= h; i++) {
        repeteCar(h-i, ' ');
        repeteCar(2*i-1, c);
        Ut.afficherSL(' ');
    }
}
void afficherNombreCroissant(int nb1, int nb2){
    for (int i = nb1; i <= nb2; i++) {
        Ut.afficher(i%10+"");
    }
}
void afficherNombreDecroissant(int nb1, int nb2){
    for (int i = nb2; nb1 <= i; i = i - 1) {
        Ut.afficher(i%10+"");
    }
}

void pyramideElaboree(int h){
    for (int i = 0; i <= h; i++) {
        repeteCar(h-i, ' ');
        afficherNombreCroissant(i, i+i-1);
        afficherNombreDecroissant(i, i+i-2);
        Ut.sautLigne();

    }
}

int nbChiffres(int n){
    int nb = 1;
    if (n<10){
        return 1;
    }
    while (n >= 10) {
        n = n / 10;
        nb = nb + 1;
    }
    return nb;
}

int nbChiffresDuCarre(int N){
    N = N*N;
    return nbChiffres(N);
}

boolean nombresAmis(int p, int q){
    int sommeP = 0;
    int sommeQ = 0;
    for (int i = 1; i < p; i++) {
        if (p % i == 0) {
            sommeP += i;
        }
    }
    for (int i = 1; i < q; i++) {
        if (q % i == 0) {
            sommeQ += i;
        }
    }
    return sommeP == q && sommeQ == p;
}

void coupleAmis(int n) {
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < i; j++) {
            if (nombresAmis(i, j)) {
                Ut.afficher(j);
                Ut.afficherSL(" et "+i);

            }
        }
    }
}

int racineParfaite(int c){
    for (int i = 1; i <= c; i++) {
        if (i*i == c){
            return i;
        }
    }
    return 0;
}

boolean estCarreParfait(int c){
    if (racineParfaite(c) != 0) {
        return true;
    }
    return false;
}

void coteAngleDroit(int a, int b){
    int cote1 = a*a;
    int cote2 = b*b;
    if (estCarreParfait(cote1) && estCarreParfait(cote2)) {
        Ut.afficher(a+" et "+b+" peuvent être les côtés de l'angle droit d'un triangle.");
    }
    else{
        Ut.afficher("Ils ne peuvent pas");
    }
}

void nbTriangleRectangle(int n){
    int nb = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            int cote1 = i * i;
            int cote2 = j * j;
            int cote3 = cote1 * cote1 + cote2 * cote2;
            cote3 = racineParfaite(cote3);
            if (n > cote1 + cote2 + cote3){
                nb ++;
            }
        }
    }
    Ut.afficher("Il y a "+nb+" triangle possible");
}

boolean estSyracusien1(int n){
    while(n != 1) {
        if (n % 2 == 0){
            n = n / 2;
            Ut.afficherSL("Il devient "+n);
        }
        else {
            n = 3 * n + 1;
            Ut.afficherSL("Il devient "+n);
        }
    }
    return n==1;
}

boolean estSyracusien2(int n, int nbMaxOp){
    for (int i = 1; i <= nbMaxOp; i++) {
        if (n % 2 == 0){
            n = n / 2;
            Ut.afficherSL("Il devient "+n);
        }
        else {
            n = 3 * n + 1;
            Ut.afficherSL("Il devient "+n);
        }
        if (n == 1){
            return true;
        }
    }

    return false;
}

void main(){
    Ut.afficher(estSyracusien2(13, 9));
}
