void echantillonRandom() {
    float nb1_50 = 0;
    float nb51_70 = 0;
    float nb71_90 = 0;
    float nb91_100 = 0;
    for(int i = 0; i <= 1000; i++){
        int r = Ut.randomMinMax( 0, 100);
        Ut.afficherSL(r);
        if (r >= 91){
            nb91_100 ++;
        }
        else if (r >= 71){
            nb71_90 ++;
        }
        else if (r >= 51){
            nb51_70 ++;
        }
        else {
            nb1_50 ++;
        }
    }
    Ut.afficherSL("Il y a "+nb1_50/10+"% entre 1 et 50, "+nb51_70/10+"% entre 51 et 70 "+nb71_90/10+"% entre 71 et 90 "+nb91_100/10+"% entre 91 et 100");
}

int random() {
    return Ut.randomMinMax(1, 100);
}

void affichePlanche(int lar, int lon, int posX, int posY) {
    for (int i = 1; i <= lon; i++) {
        for (int j = 1; j <= lar; j++) {
            if (i == posY && j == posX) {
                Ut.afficher("|o");
            }
            else {
                Ut.afficher("|-");
            }
        }
        Ut.afficherSL("|");
    }

}

boolean arivobato(int lar, int lon, boolean trace){
    int x = (lar / 2) +1;
    int y = 0;
    int r = random();
    while(x >= 0 && x < lar && y < lon){
        if (r >= 91){
            y --;
        }
        else if (r >= 71){
            x --;
        }
        else if (r >= 51){
            x ++;
        }
        else {
            y ++;
        }
        r = random();
        if (trace == true){
            Ut.afficherSL("("+y+":"+x+")");
            affichePlanche(lar, lon, x, y);
            Ut.pause(700);
            Ut.clearConsole();
        }

    }
    if (y  == lon){
        Ut.afficherSL("Il est arrivé au bateau.");
        return true;
    }
    else  {
        Ut.afficherSL("Il est tombé.");
    }
    return false;
}

void probaEmpirique(int lar, int lon, int nbMarins){
    float nbReussite = 0;
    for (int i = 0; i < 100; i++) {
        if (arivobato(lar, lon, false)){
            nbReussite ++;
        }
    }
    nbReussite = nbReussite / nbMarins;
    Ut.afficherSL(nbReussite);


}
void main(){
    probaEmpirique(7, 10, 100);
}